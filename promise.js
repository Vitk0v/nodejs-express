console.log('START !');
const delay = (time) => {
    console.log('Enter into DELAY ');
    return new Promise((resolve, reject) => {
        setTimeout(resolve, time);
    });
};

const timeout = (ms, promise) => {
    console.log('Enter into TIMEOUT');
    return new Promise((resolve, reject) => {
        promise.then(resolve);
        setTimeout(() => {
            reject('error');
        }, ms);
    });
}

// delay(1000).then(function () {
//     console.log('1 seconds have passed!')
// });

// timeout(8000, delay(5000))
//     .then(function () {
//         console.log('5 seconds have passed!')
//     })
//     .catch(function (reason) {
//         console.error('Error or timeout', reason);
//     });

// let vitkov = new Promise((resolve, reject) => {
//     setTimeout(() => resolve("OK Antoine"), 3000);
//     setTimeout(() => reject("ECHEC antoine"), 2000);
// });
//
// const marion = (number = 0) => {
//     return new Promise((resolve, reject) => {
//             number !== 0 ? resolve(`OK Marion : ${number}`) : reject("ECHEC marion");
//         }
//     )
// };

// marion(45)
//     .then(() => {
//         console.log("marion ok");
//     })
//     .catch(() => {
//         console.log("marion PAS OKK")
//     })

// Promise.all([vitkov, marion()])
//     .then((value) => {
//         console.log(`YES : ${value}`);
//     })
//     .catch((error) => {
//         console.log(`NOOOO : ${error}`);
//     })

// Promise.allSettled([vitkov, marion()])
//     .then((values) => {
//         values.forEach((value) => {
//             console.log(value);
//         })
//     })

import fetch from "node-fetch";

function fetchPoke() {
    return new Promise((resolve, reject) => {
        fetch('https://pokeapi.co/api/v2/pokemon?limit=9')
            .then((response) => response.json())
            .then((json) => json.results)
            .then((results) => {
                let pokemon = [];
                for (const ITEM of results) {
                    pokemon.push(ITEM.name);
                }
                console.log(pokemon)
                resolve(pokemon);
            })
            .catch((error) => {
                reject(`FETCH ERROR : ${error}`);
            })
    })
}

async function getName() {

    try {
        return await fetchPoke();
    } catch (e) {
        throw new Error(`CATCH ERROR : ${e}`)
    }

    // fetchPoke()
    //     .then((result) => {
    //         //console.log(result);
    //         let pokemon = [];
    //         for (let item of result) {
    //             pokemon.push(item.toUpperCase());
    //         }
    //         console.log(pokemon);
    //     })
    //     .catch(error => error)
}

//await getName();

function fetchType() {
    return new Promise((resolve, reject) => {
        fetch("https://pokeapi.co/api/v2/type")
            .then((response) => response.json())
            .then((json) => json.results)
            .then( (results) => {
                // let type = [];
                // for (const result of results) {
                //     type.push(result.name);
                // }
                //resolve(type);
                resolve(results)
            })
            .catch((error) => {
                reject(`Error : ${error}`)
            })
    })
}
//console.log(await fetchType());

async function mappingType(type) {
    const typeList = await fetchType();
    let url = "";
    for (const objectType of typeList) {
        if (objectType.name === type) {
            url = objectType.url;
        }
    }
    return url;
}

const typeID = await mappingType('water');
console.log(typeID);

console.log('FINISH !!');