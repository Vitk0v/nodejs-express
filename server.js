import http from 'http';
import app from "./app.js";

const SERVER = http.createServer(app);

SERVER.listen(3000);