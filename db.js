import mariadb from 'mariadb'

const pool = mariadb.createPool({
    host: 'mariadb',
    user: 'root',
    password: 'toor',
    database: 'pokedex'
})

export default pool